#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2022 Endless Mobile, Inc.
#
# SPDX-License-Identifier: GPL-3.0+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Environmental statistics query script for GitLab.

Usage:
    gitlab-stats.py -H https://gitlab.gnome.org/ -t <TOKEN> GNOME/glib --start-date 2022-04-01
then analyse the output and see where you might be able to reduce the energy
use of your project.

The -H and -t arguments can be omitted by putting the following in
`~/.config/gitlab-stats.ini`:
    [gitlab-stats]
    default-hostname = https://gitlab.gnome.org/
    [https://gitlab.gnome.org/]
    token = <TOKEN>

To generate an authentication token for the script, go to
    https://gitlab.gnome.org/-/profile/personal_access_tokens
and generate a token with the `read_api` scope.

To install the dependencies for this script:
    pip3 install python-gitlab
"""

import argparse
from collections import namedtuple
import configparser
import datetime
import dateutil.parser
import functools
import heapq
import itertools
import operator
import os
import pickle
import statistics
import sys
import gitlab

import gi
from gi.repository import GLib  # noqa: E402


def cut_by_field(objects, field, lower_bound, upper_bound):
    return itertools.takewhile(lambda obj: dateutil.parser.isoparse(obj.attributes[field]) >= lower_bound,
                               itertools.dropwhile(lambda obj: dateutil.parser.isoparse(obj.attributes[field]) > upper_bound, objects))


def j_to_kwh(joules):
    return joules / (60 * 60 * 1000)


class JobData:
    def __init__(self, duration=0.0, artifact_storage=0, energy=0.0):
        self._duration = duration
        self._artifact_storage = artifact_storage
        self._energy = energy

    @property
    def duration(self):
        return self._duration

    @property
    def artifact_storage(self):
        return self._artifact_storage

    @property
    def energy(self):
        return self._energy

    def __add__(self, other):
        return JobData(self.duration + other.duration,
                       self.artifact_storage + other.artifact_storage,
                       self.energy + other.energy)


def job_calculate_data(job):
    # The duration is in seconds but may be absent if a job hasn’t run.
    if not job.duration:
        return JobData()

    # FIXME: Here we assume that each CI runner is a 1000kWh/year computer, exclusively
    # running this CI job for the duration of the job.
    #
    # VMWare says (https://www.vmware.com/uk/company/sustainability/carbon-calculator.html)
    # one VM is 1000kWh/year, which is 114W at 24h/day all year.
    #
    # Some runners may actually run multiple jobs in parallel in Docker
    # containers (see https://docs.gitlab.com/runner/configuration/advanced-configuration.html).
    # If so, this cost should be amortised by the parallelisation factor.
    # See: https://gitlab.gnome.org/Infrastructure/GitLab/-/issues/574
    return JobData(job.duration,
                   functools.reduce(operator.add, [a['size'] for a in job.attributes['artifacts']], 0),
                   j_to_kwh(job.duration * 114))


def main():
    # Load our GitLab authentication details.
    config = configparser.ConfigParser()
    config_path = os.path.join(GLib.get_user_config_dir(),
                               'gitlab-stats.ini')
    config.read(config_path)

    gl_default_hostname = None
    gl_default_token = None

    try:
        gl_default_hostname = config['gitlab-stats']['default-hostname']
        gl_default_token = config[gl_default_hostname]['token']
        write_config = False
    except KeyError:
        write_config = True

    # Parse command line arguments.
    parser = argparse.ArgumentParser(
        description='Query environmental statistics for a project which uses '
                    'GitLab. The statistics will be outputted on stdout.')
    parser.add_argument('gl_project', metavar='<gitlab project>',
                        help='GitLab project to query '
                             '(for example: ‘GNOME/glib’)')
    parser.add_argument('--start-date', metavar='<ISO 8601 date/time>',
                        nargs='?',
                        type=dateutil.parser.isoparse,
                        help='start date to query from '
                             '(for example: ‘2022-04-01’; default: last week)')
    parser.add_argument('--end-date', metavar='<ISO 8601 date/time>',
                        nargs='?',
                        type=dateutil.parser.isoparse,
                        help='end date to query to '
                             '(for example: ‘2022-05-01’; default: now)')
    parser.add_argument('-C', dest='path', default='.',
                        help='repository to use (default: current directory)')

    parser.add_argument('-H', '--hostname', default=gl_default_hostname,
                        required=(gl_default_hostname is None),
                        help='GitLab hostname (for example: '
                             '‘https://gitlab.gnome.org/’, default: '
                             'load from {})'.format(config_path))
    parser.add_argument('-t', '--token', default=None, required=False,
                        help='GitLab authentication token (default: '
                             'load from {})'.format(config_path))

    args = parser.parse_args()

    # Work out the dates.
    if not args.start_date:
        args.start_date = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=7)
    if not args.end_date:
        args.end_date = datetime.datetime.now(datetime.timezone.utc)

    # Try and look up the token for the given hostname, if not given on the
    # command line.
    try:
        if not args.token:
            args.token = config[args.hostname]['token']
    except KeyError:
        parser.error('Configuration key {}.{} not found in {}'.format(
                     args.hostname, 'token', config_path))
        sys.exit(1)

    # Authenticate with GitLab to check the config.
    try:
        gl = gitlab.Gitlab(args.hostname, private_token=args.token)
        gl_project_name = args.gl_project
        gl_project = gl.projects.get(gl_project_name)
        # Normalize project name (eg gnome/foo → GNOME/foo)
        gl_project_name = gl_project.path_with_namespace
    except gitlab.exceptions.GitlabAuthenticationError as e:
        print('Authentication error: ' + e.error_message)
        print('Your token may have expired. Check and refresh it at:')
        print(f'{args.hostname}-/profile/personal_access_tokens')
        print('It will need the `read_api` scope.')
        sys.exit(1)

    if write_config:
        if 'gitlab-stats' not in config.sections() or \
           'default-hostname' not in config['gitlab-stats']:
            config['gitlab-stats'] = {
                'default-hostname': args.hostname,
            }

        config[args.hostname] = {
            'token': args.token,
        }

        with open(os.open(config_path, os.O_CREAT | os.O_WRONLY, 0o600), 'w') as fh:
            config.write(fh)

    # Load cached data, or query again
    try:
        # FIXME: Invalidate the cache if the query parameters have changed or
        # time has passed.
        with open('data.bin', 'rb') as fh:
            (cache_hostname, cache_gl_project, cache_start_date, cache_end_date, forks, jobs) = pickle.load(fh)

        if (cache_hostname != args.hostname or
            cache_gl_project != args.gl_project or
            cache_start_date != args.start_date or
            cache_end_date != args.end_date):
          # Cache is invalid, query again
          raise Exception()
    except:
        print('Querying for jobs…')

        # Query the project and list all the forks of it which have been active recently.
        #
        # API reference:
        #  - https://docs.gitlab.com/ee/api/projects.html
        #  - https://python-gitlab.readthedocs.io/en/stable/api-usage.html
        forks = gl_project.forks.list(
            as_list=False,
            order_by='last_activity_at',
            all=True)
        forks = cut_by_field(forks, 'last_activity_at', lower_bound=args.start_date, upper_bound=args.end_date)
        forks = list(map(lambda fork: gl.projects.get(fork.id, lazy=True), forks))

        # For each fork (and the project itself), lazily list all the CI jobs, sorted by creation time.
        # Importantly, each element of `jobs_by_fork` here is a generator, as the result lists can be very long.
        jobs_by_fork = []
        for project in itertools.chain([gl_project], forks):
            try:
                jobs_by_fork.append(project.jobs.list(
                    as_list=False,
                    order_by='created_at',
                    all=True))
            except gitlab.exceptions.GitlabListError:
                print('Failed to query jobs for project ID {0}, ignoring'.format(project.id))
                pass
        jobs_by_fork = list(jobs_by_fork)

        # Merge all the generator elements in `jobs_by_fork` into a single generator, also sorted by creation time.
        # Then trim the generator by the upper and lower bounds, and flatten it to a list. (Otherwise it can only
        # be iterated over once.)
        jobs_generator = heapq.merge(*jobs_by_fork, key=lambda job: job.attributes['created_at'], reverse=True)
        jobs = list(cut_by_field(jobs_generator, 'created_at', lower_bound=args.start_date, upper_bound=args.end_date))

        with open('data.bin', 'wb') as fh:
           pickle.dump((args.hostname, args.gl_project, args.start_date, args.end_date, forks, jobs), fh)

    # Calculate totals
    job_data = [job_calculate_data(job) for job in jobs]
    totals = functools.reduce(JobData.__add__, job_data, JobData())
    durations = [job.duration for job in job_data]
    storages = [job.artifact_storage for job in job_data]

    print('Between {} and {}, {} and its {} forks used:'.format(args.start_date, args.end_date, args.gl_project, len(forks)))

    if not jobs:
        print(' - No CI jobs were run!')
        return

    print(" - {0} CI jobs, totalling {1:.0f} minutes (duration minimum {2:.1f}, median {3:.1f}, maximum {4:.1f})".format(
        len(jobs),
        totals.duration / 60.0,
        min(durations) / 60.0,
        statistics.median(durations) / 60.0,
        max(durations) / 60.0))
    print(" - Total energy use: {0:.2f}kWh".format(totals.energy))
    print(" - Total artifact storage: {0:.0f} MB (minimum {1:.1f}, median {2:.1f}, maximum {3:.1f})".format(
        totals.artifact_storage / (1000 * 1000),
        min(storages) / (1000 * 1000),
        statistics.median(storages) / (1000 * 1000),
        max(storages) / (1000 * 1000)))

    # FIXME: Ideas for other things to account for:
    #  - Total disk usage of container registries
    #    Doesn’t seem to work as https://docs.gitlab.com/ee/api/container_registry.html#get-details-of-a-single-repository would suggest
    #    print(gl_project.repositories.get(gl_project.repositories.list()[0].id))
    #  - Bandwidth usage of all repo pulls/GitLab page loads. Doesn’t seem
    #    possible to query that using the GitLab API.
    #  - Disk usage of the git repository. Not straightforward and fairly low
    #    impact for most repositories.


if __name__ == '__main__':
    main()
