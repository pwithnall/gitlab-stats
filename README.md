gitlab-stats
===

Environmental statistics query script for GitLab.

This uses the GitLab API to work out the energy and resource usage of a project’s development on GitLab.
It sums the durations of pipelines and their storage requirements, and uses that to estimate energy use.
It does this across the forks of a project.

The energy models it uses are quite basic, and the data it gathers is not exhaustive, but it should give an overview of a project’s impact, useful for comparing over time, or comparing with other projects on the same server.

Why do this?
---

The energy used in developing software can be considered as the ‘embodied energy’ (and hence embodied carbon emissions) in each release of the software.
This is a contributor to the overall emissions of the software project, although typically will be secondary to the emissions caused by use of the software.
Still, for larger software projects it may be possible to easily reduce the embodied energy of each release, and hence the impacts of the project as a whole by a small amount.

You should probably still put more effort into optimising the performance (and hence power usage) of the software in use though.

Some background reading:
 - https://tecnocode.co.uk/misc/talks/guadec-2020/presentation.pdf
 - https://tecnocode.co.uk/misc/talks/guadec-2020/presentation_notes.pdf

What can I do with the results?
---

Use them to compare projects and see where the low-hanging fruit for reducing CI resource usage might be.

The most straightforward improvements to make are generally to speed up CI pipelines. This reduces resource consumption and makes project development easier (less waiting for CI pipelines to complete).

Deleting job artifacts after a timeout (for example, after 7 days) similarly generally has few downsides and reduces artifact storage use.

Other improvements to make would be to reduce the number of CI jobs running by default, perhaps by moving some onto a schedule (rather than on every commit) or making some manual.
This may make development a bit less convenient, but will reduce resource usage. You will need to work out the tradeoff point. Storing fewer artifacts has similar cost/convenience tradeoffs.

Usage
---

```
gitlab-stats.py -H https://gitlab.gnome.org/ -t <TOKEN> GNOME/glib --start-date 2022-04-01T00:00:00Z --end-date 2022-04-08T00:00:00Z
```
then analyse the output and see where you might be able to reduce the energy use of your project.

The `-H` and `-t` arguments can be omitted by putting the following in `~/.config/gitlab-stats.ini`:
```
[gitlab-stats]
default-hostname = https://gitlab.gnome.org/
[https://gitlab.gnome.org/]
token = <TOKEN>
```

The `--start-date` and `--end-date` arguments can be omitted to just query for the last 7 days. Querying for durations longer than a month,
or for shorter durations on projects which have a lot of CI jobs or a lot of forks, could take many minutes to complete.

To generate an authentication token for the script, go to https://gitlab.gnome.org/-/profile/personal_access_tokens and generate a token with the `read_api` scope.

Installation
---

To install the dependencies for this script:
```
pip3 install python-gitlab
```

Contributing
---

All contributions are welcome! The script is written in Python and uses REST APIs provided by GitLab.

Contributions to improve the usability of the script, fix corner cases, output downloaded data as a CSV for further analysis, or improve the energy models the script uses are gratefully received.

While the script is currently targeted at the GitLab instance used by the GNOME project, it is not tied to GNOME and contributions are welcome to make the script more useful for GitLab instances used by other FOSS projects.
